import telegram
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates 
import seaborn as sns
import io
import pandas as pd
import pandahouse
from read_db.CH import Getch
import os

sns.set()

link_chart = 'http://superset.lab.karpov.courses/r/241'
link_dashboard = 'http://superset.lab.karpov.courses/r/236'

link_chart_ctr = 'http://superset.lab.karpov.courses/r/240'
link_dashboard_ctr = 'http://superset.lab.karpov.courses/r/239'

link_chart_message = 'http://superset.lab.karpov.courses/r/243'
link_dashboard_message = 'http://superset.lab.karpov.courses/r/242'

with open('token_telegram_bot.txt', 'r+') as f:
    token = f.read()

def test_alert(chat=None):
    chat_id = -664596965  
    bot = telegram.Bot(token=token)
    
    data = Getch(
    '''
    select 
        toStartOfInterval(time, INTERVAL 15 minute) as interval,
        count(distinct user_id) as count_unique
    from simulator.feed_actions
    inner join simulator.message_actions on simulator.feed_actions.user_id = simulator.message_actions.user_id
    where interval >= today() - 1 and interval < toStartOfInterval(now(), INTERVAL 15 minute)
    group by interval
    order by interval
    '''
    ).df
  
    data['level_1'] = data.count_unique.rolling(window=15, min_periods=1, closed='left').mean() - data.count_unique.std()*1
    data['level_2'] = data.count_unique.rolling(window=15, min_periods=1, closed='left').mean() + data.count_unique.std()*1
       
    gap = data.count_unique[data.index.max()]  
    
    if data.count_unique[data.index.max()] > data.level_2[data.index.max()]:
        dev = (gap / data.level_2[data.index.max()] - 1)

        msg = f'Метрика: кол-во уникальных пользователей в срезе {data.interval[data.index.max()]}:\n\
        Текущее значение: {gap}\n\
        Отклонение: {dev:.0%}\n\
        Чарт метрики: {link_chart}\n\
        Дашборд: {link_dashboard}'

        bot.sendMessage(chat_id=chat_id, text=msg)
    
    elif data.count_unique[data.index.max()] < data.level_1[data.index.max()]:
        dev = (gap / data.level_1[data.index.max()] - 1)

        msg = f'Метрика: кол-во уникальных пользователей в срезе {data.interval[data.index.max()]}:\n\
        Текущее значение: {gap}\n\
        Отклонение: {dev:.0%}\n\
        Чарт метрики: {link_chart}\n\
        Дашборд: {link_dashboard}'

        bot.sendMessage(chat_id=chat_id, text=msg)

    if data.count_unique[data.index.max()] > data.level_2[data.index.max()] or\
       data.count_unique[data.index.max()] < data.level_1[data.index.max()]:

        # зададим размер фигуры
        plt.figure(figsize=(16,12))
        # зададим формат даты
        myFmt = mdates.DateFormatter('%d-%H-%M')
        # создадим объект оси
        axes = plt.gca()

        sns.lineplot(data=data, x='interval', y='level_2', label='level_2', linestyle='--')
        sns.lineplot(data=data, x='interval', y='count_unique', label='count_unique', linewidth=3.0)
        sns.lineplot(data=data, x='interval', y='level_1', label='level_1', linestyle='--')

        axes.set_title('Количество актиыных пользователей\n', fontsize=16)
        axes.xaxis.set_major_formatter(myFmt)

        plt.xlabel('\ninterval', fontsize=14)
        plt.ylabel('count_unique\n', fontsize=14)
        plt.grid(True);

        # зададим файловый объект
        plot_object = io.BytesIO()
        # сохраним в него наш график
        plt.savefig(plot_object)
        # зададим имя нашему файловому объекту
        plot_object.name = 'alert_plot.png'
        # перенесем курсор из конца файлового объекта в начало, чтобы прочитать весь файл
        plot_object.seek(0)
        # закроем файл
        plt.close()
        # отправим изображение
        bot.sendPhoto(chat_id=chat_id, photo=plot_object)


    data_ctr = Getch(
    '''
    select 
        toStartOfInterval(time, INTERVAL 15 minute) as interval,
        countIf(user_id, action='like') / countIf(user_id, action='view') as CTR
    from simulator.feed_actions
    where interval >= today() - 1 and interval < toStartOfInterval(now(), INTERVAL 15 minute)
    group by interval
    order by interval
    '''
    ).df
  
    data_ctr['level_1'] = data_ctr.CTR.rolling(window=7, min_periods=1, closed='left').mean() - data_ctr.CTR.std()*1.2
    data_ctr['level_2'] = data_ctr.CTR.rolling(window=7, min_periods=1, closed='left').mean() + data_ctr.CTR.std()*1.2
    
    gap_ctr = data_ctr.CTR[data_ctr.index.max()]
    
    if data_ctr.CTR[data_ctr.index.max()] > data_ctr.level_2[data_ctr.index.max()]:    
        dev_ctr = (gap_ctr / data_ctr.level_2[data_ctr.index.max()] - 1)

        msg_ctr = f'Метрика: CTR в срезе {data_ctr.interval[data_ctr.index.max()]}:\n\
        Текущее значение: {round(gap_ctr, 2)}\n\
        Отклонение: {dev_ctr:.1%}\n\
        Чарт метрики: {link_chart_ctr}\n\
        Дашборд: {link_dashboard_ctr}'

        bot.sendMessage(chat_id=chat_id, text=msg_ctr)
    
    elif data_ctr.CTR[data_ctr.index.max()] < data_ctr.level_1[data_ctr.index.max()]:
        dev_ctr = (gap_ctr / data_ctr.level_1[data_ctr.index.max()] - 1)

        msg_ctr = f'Метрика: CTR в срезе {data_ctr.interval[data_ctr.index.max()]}:\n\
        Текущее значение: {round(gap_ctr, 2)}\n\
        Отклонение: {dev_ctr:.1%}\n\
        Чарт метрики: {link_chart_ctr}\n\
        Дашборд: {link_dashboard_ctr}'

        bot.sendMessage(chat_id=chat_id, text=msg_ctr)

    if data_ctr.CTR[data_ctr.index.max()] > data_ctr.level_2[data_ctr.index.max()] or\
       data_ctr.CTR[data_ctr.index.max()] < data_ctr.level_1[data_ctr.index.max()]:

        plt.figure(figsize=(16,12))
        myFmt = mdates.DateFormatter('%d-%H-%M')
        axes = plt.gca()

        sns.lineplot(data=data_ctr, x='interval', y='level_2', label='level_2', linestyle='--')
        sns.lineplot(data=data_ctr, x='interval', y='CTR', label='CTR', linewidth=3.0)
        sns.lineplot(data=data_ctr, x='interval', y='level_1', label='level_1', linestyle='--')

        axes.set_title('CTR\n', fontsize=16)
        axes.xaxis.set_major_formatter(myFmt)

        plt.xlabel('\ninterval', fontsize=14)
        plt.ylabel('CTR\n', fontsize=14)
        plt.grid(True);

        plot_object = io.BytesIO()
        plt.savefig(plot_object)
        plot_object.name = 'alert_plot.png'
        plot_object.seek(0)
        plt.close()
        bot.sendPhoto(chat_id=chat_id, photo=plot_object)
        

    data_message = Getch(
    '''
    select 
        toStartOfInterval(time, INTERVAL 15 minute) as interval,
        count(user_id) as count_message
    from simulator.message_actions
    where interval >= today() - 1 and interval < toStartOfInterval(now(), INTERVAL 15 minute)
    group by interval
    order by interval
    '''
    ).df
  
    data_message['level_1'] = data_message.count_message.rolling(window=10, min_periods=1, closed='left').mean()\
                        - data_message.count_message.std()*1.2
    data_message['level_2'] = data_message.count_message.rolling(window=10, min_periods=1, closed='left').mean()\
                        + data_message.count_message.std()*1.2
    
    gap_message = data_message.count_message[data_message.index.max()]
    
    if data_message.count_message[data_message.index.max()] > data_message.level_2[data_message.index.max()]:    
        dev_message = (gap_message / data_message.level_2[data_message.index.max()] - 1)

        msg_message = f'Метрика: количество сообщений в срезе {data_message.interval[data_message.index.max()]}:\n\
        Текущее значение: {gap_message}\n\
        Отклонение: {dev_message:.0%}\n\
        Чарт метрики: {link_chart_message}\n\
        Дашборд: {link_dashboard_message}'

        bot.sendMessage(chat_id=chat_id, text=msg_message)
    
    elif data_message.count_message[data_message.index.max()] < data_message.level_1[data_message.index.max()]:
        dev_message = (gap_message / data_message.level_1[data_message.index.max()] - 1)

        msg_message = f'Метрика: количество сообщений в срезе {data_message.interval[data_message.index.max()]}:\n\
        Текущее значение: {gap_message}\n\
        Отклонение: {dev_message:.0%}\n\
        Чарт метрики: {link_chart_message}\n\
        Дашборд: {link_dashboard_message}'

        bot.sendMessage(chat_id=chat_id, text=msg_message)

    if data_message.count_message[data_message.index.max()] > data_message.level_2[data_message.index.max()] or\
       data_message.count_message[data_message.index.max()] < data_message.level_1[data_message.index.max()]:

        plt.figure(figsize=(16,12))
        myFmt = mdates.DateFormatter('%d-%H-%M')
        axes = plt.gca()

        sns.lineplot(data=data_message, x='interval', y='level_2', label='level_2', linestyle='--')
        sns.lineplot(data=data_message, x='interval', y='count_message', label='count_message', linewidth=3.0)
        sns.lineplot(data=data_message, x='interval', y='level_1', label='level_1', linestyle='--')

        axes.set_title('Количество сообщений\n', fontsize=16)
        axes.xaxis.set_major_formatter(myFmt)

        plt.xlabel('\ninterval', fontsize=14)
        plt.ylabel('count_message\n', fontsize=14)
        plt.grid(True);

        plot_object = io.BytesIO()
        plt.savefig(plot_object)
        plot_object.name = 'alert_plot.png'
        plot_object.seek(0)
        plt.close()
        bot.sendPhoto(chat_id=chat_id, photo=plot_object)
        
try:
    test_alert()
except Exception as e:
    print(e)
   